#include <stdio.h>
#include <stdlib.h>
#include <time.h>
typedef struct
{
    int Odev[4];
    int Quiz[2];
    int Sinav[3];

}Notlar;

typedef struct
{
    char id[50];
    char isim[50];
    Notlar notlar;

}Ogrenci;

int *MaxOdevBul(Ogrenci *pogrenci,int *pogrEbOdev)
{
    int i,j;

    for(i=0;i<100;i++)
    {
        for(j=0;j<4;j++)
        {
            if((pogrenci+i)->notlar.Odev[j] > *(pogrEbOdev+i))
            {
                *(pogrEbOdev+i) = (pogrenci+i)->notlar.Odev[j];
            }
        }
    }
}

int *MaxQuizBul(Ogrenci *pogrenci,int *pogrEbQuiz)
{
    int i,j;

    for(i=0;i<100;i++)
    {
        for(j=0;j<2;j++)
        {
            if((pogrenci+i)->notlar.Quiz[j] > *(pogrEbQuiz+i))
            {
                *(pogrEbQuiz+i) = (pogrenci+i)->notlar.Quiz[j];
            }
        }
    }
}

int *MaxSinavBul(Ogrenci *pogrenci,int *pogrEbSinav)
{
    int i,j;

    for(i=0;i<100;i++)
    {
        for(j=0;j<2;j++)
        {
            if((pogrenci+i)->notlar.Sinav[j] > *(pogrEbSinav+i))
            {
                *(pogrEbSinav+i) = (pogrenci+i)->notlar.Sinav[j];
            }
        }
    }
}

int NotOrtalamaBul(Ogrenci *pogrenci,int *pogrG_ort)
{
    int i,j,ortalama=0,Odev_Ortalama[100]={},Quiz_Ortalama[100]={},Sinav_Ortalama[100]={};

    for(i=0;i<100;i++)
    {
        for(j=0;j<4;j++)
        {
            ortalama += (pogrenci+i)->notlar.Odev[j];
        }
            Odev_Ortalama[i]=ortalama /4;
            ortalama=0;
    }

    ortalama=0;
    for(i=0;i<100;i++)
    {
        for(j=0;j<2;j++)
        {
            ortalama += (pogrenci+i)->notlar.Quiz[j];
        }
            Quiz_Ortalama[i]=ortalama /2;
            ortalama=0;
    }

    ortalama=0;
    for(i=0;i<100;i++)
    {
        for(j=0;j<3;j++)
        {
            ortalama += (pogrenci+i)->notlar.Sinav[j];
        }
            Sinav_Ortalama[i]=ortalama /3;
            ortalama=0;
    }

    for(i=0;i<100;i++)
    {
        *(pogrG_ort+i) = (Odev_Ortalama[i]*0.1) + (Quiz_Ortalama[i]*0.3) + (Sinav_Ortalama[i]*0.6);
    }
}

int main()
{
    int i,j;
    Ogrenci *pogrenci,ogrenci[100]={};
    int *pogrEbOdev,ebOdev[100]={},*pogrEbQuiz,ebQuiz[100]={},*pogrEbSinav,ebSinav[100],*pogrG_Ort,Genel_Ortalama[100];

    pogrenci = &ogrenci;

    NotAtama(pogrenci);

    pogrEbOdev = &ebOdev;
    pogrEbQuiz = &ebQuiz;
    pogrEbSinav = &ebSinav;
    pogrG_Ort = &Genel_Ortalama;

    MaxOdevBul(pogrenci,pogrEbOdev);
    MaxQuizBul(pogrenci,pogrEbQuiz);
    MaxSinavBul(pogrenci,pogrEbSinav);
    NotOrtalamaBul(pogrenci,pogrG_Ort);

    printf("Ogrenci No\tMaximum Odev Notu\tMaximum Quiz Notu\tMaximum Sinav Notu\tGenel Ortalama\t\tDurum \n");

    for(i=0;i<100;i++)
    {
        printf("%d \t\t\t%d \t\t\t %d\t\t\t%d\t\t\t%d\t-------->   %s\n\n",i+1,ebOdev[i],ebQuiz[i],ebSinav[i],Genel_Ortalama[i],(Genel_Ortalama[i]>60) ? "Basarili" : "Basarisiz");
    }
    getchar();
}
int NotAtama(Ogrenci *pogrenci)
{
    int i,j;

    for(i=0;i<100;i++)
    {
        for(j=0;j<4;j++)
        {
            (pogrenci+i)->notlar.Odev[j]=1+rand()%100;
        }
    }
    for(i=0;i<100;i++)
    {
        for(j=0;j<2;j++)
        {
            (pogrenci+i)->notlar.Quiz[j]=1+rand()%100;
        }
    }
    for(i=0;i<100;i++)
    {
        for(j=0;j<3;j++)
        {
            (pogrenci+i)->notlar.Sinav[j]=1+rand()%100;
        }
    }
}
